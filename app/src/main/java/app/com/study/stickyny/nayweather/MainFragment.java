package app.com.study.stickyny.nayweather;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by NN on 2016-11-24.
 */

public class MainFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static MainFragment create() {
        MainFragment mainFragment = new MainFragment();
        return mainFragment;
    }

    @Override
    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(hasMenu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh :
                NayTask nayTask = new NayTask();
                nayTask.execute("1835841");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Create fake weekly forecast data for the RecyclerView
        String[] data = {
                "Mon 11/28 - Sunny - 10/5",
                "Tue 11/29 - Cloudy - 3/0",
                "Wed 11/30 - Foggy - 6/1",
                "Thurs 12/1 - Sunny - 15/8",
                "Fri 12/2 - Rainy - 4/1",
                "Sat 12/3 - Sunny - 13/6",
                "Sun 12/3 - Cloudy - 5/1"
        };
        List<String> weekForecast = new ArrayList<>(Arrays.asList(data));

        // real forecast data
        NayTask nayTask = new NayTask();
        nayTask.execute("1835841");

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        // Initialize the RecyclerView and Adapter
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.forecast_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NayAdapter(weekForecast);
        mRecyclerView.setAdapter(mAdapter);

        return rootView;
    }
}
