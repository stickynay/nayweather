package app.com.study.stickyny.nayweather;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static app.com.study.stickyny.nayweather.BuildConfig.OPEN_WEATHER_MAP_API_KEY;

/**
 * Created by NN on 2016-11-30.
 */

public class NayTask extends AsyncTask<String, Void, Void> {
    private final String LOG_TAG = NayTask.class.getSimpleName();
    private String forecastJsonStr = null;

    public NayTask() {
        super();
    }

    @Override
    protected Void doInBackground(String... params) {
        if(weatherRequest(params[0]).length() != 0) {
            forecastJsonStr = weatherRequest(params[0]);
        } else {
            Log.e("NAY", "response string is null");
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    String weatherRequest(String cityId) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String forecastJsonStr = null;

        String format = "json";
        String units = "metric";
        int numDays = 7;

        try {
            final String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
            final String CITY_ID_PARAM = "id";
            final String FORMAT_PARAM = "mode";
            final String UNITS_PARAM = "units";
            final String DAYS_PARAM = "cnt";
            final String API_KEY_PARAM = "APPID";

            Uri uri = Uri.parse(BASE_URL).buildUpon()
                    .appendQueryParameter(CITY_ID_PARAM, cityId)
                    .appendQueryParameter(FORMAT_PARAM, format)
                    .appendQueryParameter(UNITS_PARAM, units)
                    .appendQueryParameter(DAYS_PARAM, Integer.toString(numDays))
                    .appendQueryParameter(API_KEY_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();
            URL url = new URL(uri.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if(inputStream == null) {
                forecastJsonStr = null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0) {
                forecastJsonStr = null;
            }
            forecastJsonStr = buffer.toString();
        } catch(Exception e) {
            e.printStackTrace();
            forecastJsonStr = null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Log.i(LOG_TAG, forecastJsonStr);
        return forecastJsonStr;
    }
}
